import {onMounted, ref} from "vue";
import http from "@/utils/request";
import { ElMessage } from 'element-plus'
export function useDelete(functionMethod,getSearchList) {

    let selectId = ref({})

    let dialogVisible = ref(false)

    const deleteData=(row)=> {
         selectId.value = row.id;
        dialogVisible.value = true;
    }
    const handleDelete=async () => {
        dialogVisible.value = false;
        if (selectId.value) {
            let params = {};
            params.id = selectId.value;
            let res = await  http.post(functionMethod, params);
            if (res.code === 200) {
                ElMessage({
                    message: '删除成功',
                    type: 'success',
                })
                 getSearchList();
            } else {
                ElMessage.error(res.msg)
            }
        }
    }
    return {dialogVisible, deleteData,handleDelete}

}