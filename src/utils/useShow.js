import {useRoute, useRouter} from "vue-router";
import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function useShow(functionMethod,getSearchList) {

    const router = useRouter();

    const route = useRoute()
    const viewModel = ref({})

    onMounted(async () => {
        let id = route.query.id;
        let data = {};
        data.id = id;
        let res = await  http.post(functionMethod, data);
        if (res.code === 200) {
            viewModel.value = res.data;
        }
    })
    const  goBack=()=> {
         router.go(-1);
    }
    return {viewModel,goBack}

}
