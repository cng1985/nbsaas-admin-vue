export default [{

    name: "system_home",
    path: "/system/layout",
    component: () => import("../../views/common/system/layout.vue"),
    meta: {
        title: "菜品管理",
        icon: "el-icon-platform-eleme"
    },
    children: [{
        name: "system_index",
        path: "/system/index",
        component: () => import("../../views/common/system/index.vue"),
        meta: {
            title: "系统信息",
            icon: "el-icon-platform-eleme"
        }
    }, {
        name: "system_setting",
        path: "/system/setting",
        component: () => import("../../views/common/system/setting.vue"),
        meta: {
            title: "系统设置",
            icon: "el-icon-platform-eleme"
        }
    }]
}

]
