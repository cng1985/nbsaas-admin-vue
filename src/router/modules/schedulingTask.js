export default [
    {
        name: "schedulingTask_home",
        path: "/schedulingTask/index",
        component: () => import("@/views/pages/schedulingTask/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTask_add",
        path: "/schedulingTask/add",
        component: () => import("@/views/pages/schedulingTask/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTask_update",
        path: "/schedulingTask/update",
        component: () => import("@/views/pages/schedulingTask/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTask_layout",
        path: "/schedulingTask/view_layout",
        component: () => import("@/views/pages/schedulingTask/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children: [
            {
                name: "schedulingTask_view",
                path: "/schedulingTask/view",
                component: () => import("@/views/pages/schedulingTask/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "schedulingTask_view_record",
                path: "/schedulingTask/view_record",
                component: () => import("@/views/pages/schedulingTask/view_record.vue"),
                meta: {
                    title: "执行记录",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]
