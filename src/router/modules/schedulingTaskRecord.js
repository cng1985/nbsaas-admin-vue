export default [
    {
        name: "schedulingTaskRecord_home",
        path: "/schedulingTaskRecord/index",
        component: () => import("@/views/pages/schedulingTaskRecord/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTaskRecord_add",
        path: "/schedulingTaskRecord/add",
        component: () => import("@/views/pages/schedulingTaskRecord/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTaskRecord_update",
        path: "/schedulingTaskRecord/update",
        component: () => import("@/views/pages/schedulingTaskRecord/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "schedulingTaskRecord_view",
        path: "/schedulingTaskRecord/view",
        component: () => import("@/views/pages/schedulingTaskRecord/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    }
]