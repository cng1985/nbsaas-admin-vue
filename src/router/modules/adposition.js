export default [
    {
        name: "adPosition_home",
        path: "/adPosition/index",
        component: () => import("@/views/pages/adPosition/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adPosition_add",
        path: "/adPosition/add",
        component: () => import("@/views/pages/adPosition/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adPosition_update",
        path: "/adPosition/update",
        component: () => import("@/views/pages/adPosition/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "adPosition_layout",
        path: "/adPosition/view_layout",
        component: () => import("@/views/pages/adPosition/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "adPosition_view",
                path: "/adPosition/view",
                component: () => import("@/views/pages/adPosition/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]