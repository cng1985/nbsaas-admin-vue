export default [
    {
        name: "structure_home",
        path: "/structure/index",
        component: () => import("@/views/pages/structure/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]