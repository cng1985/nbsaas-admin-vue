export default [
    {
        name: "dict_home",
        path: "/dict/index",
        component: () => import("@/views/pages/dict/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dict_add",
        path: "/dict/add",
        component: () => import("@/views/pages/dict/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dict_update",
        path: "/dict/update",
        component: () => import("@/views/pages/dict/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "dict_view",
        path: "/dict/view",
        component: () => import("@/views/pages/dict/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    }
]