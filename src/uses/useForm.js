import http from "@/utils/request";
import {ElMessage} from "element-plus";

export function useForm(model,form,ruleForm,mode,afterCallBack) {


    const operateData = async () => {
        try {
            let valid = await ruleForm.value.validate();
            if (!valid) {
                return;
            }
        } catch (e) {
            return;
        }

        let res = await http.post(`/${model}/${mode}`, form.value);
        if (res.code !== 200) {
            ElMessage.error(res.msg)
            return
        }

        ElMessage({
            message: '更新数据成功',
            type: 'success',
        })

        if (afterCallBack){
            afterCallBack();
        }

    }
    return {operateData}

}
