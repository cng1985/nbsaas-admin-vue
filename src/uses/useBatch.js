import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function useBatch(search,initData,callBack) {

    const  data=ref(initData);
    onMounted(async () => {
        let res = await http.post(`/json/batch`, search);
        if (res.code === 200) {
            data.value = res.data;
            if (callBack){
                callBack(res.data);
            }
        }
    })

    return {data}

}
