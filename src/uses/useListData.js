import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function useListData(model,param) {

    const loading=ref(false);
    const listData=ref([]);

    onMounted(async () => {
        await loadDate();
    })

    const loadDate=async () => {
        let data = {};
        data.model = model;
        data.filters = param;
        let res = await http.post(`/data/list`, data);
        if (res.code === 200) {
            listData.value = res.data;
        }
    }


    return {listData,loading,loadDate}

}
