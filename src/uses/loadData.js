import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function loadData(url, param) {
	//集合
	let data = ref({})

    const loadData=async ()=>{
		let res = await http.post(url, param);
		if (res.code === 200) {
			data.value=res.data;
		}

	}
	onMounted(async () => {
		await loadData()
	})
	return {
		data,loadData
	}
}

