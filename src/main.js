import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import 'element-plus/dist/index.css'
import './assets/main.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
import nbsaas from './nbsaas'
import ElementPlus from 'element-plus'
const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(nbsaas)
app.use(ElementPlus, {
    locale: zhCn,
})

app.mount('#app')
app.config.warnHandler = (msg, instance, trace) => {}
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
