//图标选择器配置
export default {
	icons: [{
			name: '默认',
			icons: [
				"fa fa-home",
				"fa  fa-gear",
				"fa  fa-comments",
				'fa fa-users',
				'fa fa-user',
				'fa fa-wallet',
				'fa fa-regular fa-envelope',
				'fa fa-thin fa-compass',
				'fa fa-solid fa-store',
				'fa fa-solid fa-cart-shopping',
				'fa fa-solid fa-book-journal-whills',
				'fa fa-solid fa-shop',
				'fa fa-solid fa-bars'
			]
		},
		{
			name: '扩展',
			icons: [
				'fa fa-home',
				'fa fa-users',
				'fa fa-user',
				'fa fa-list',
				'fa-regular fa-circle',
				'fa fa-light fa-life-ring',
			]
		}
	]
}
