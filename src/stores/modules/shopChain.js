import {defineStore} from 'pinia'

export const searchStore = defineStore('shopChain', {

    state: () => {
        return {
            searchObject: {
                no: 1,
                size: 10,
                name: '',
                shopCatalog: '',
                address: '',
                shopState: '',
                shopType:"chain"
            }
        }
    },
    getters: {
        searchData: (state) => state.searchObject,
    },
    actions: {
        updateSearchObject(obj) {
            this.searchObject = obj;
        }
    },
})
