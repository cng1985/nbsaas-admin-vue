import { defineStore } from 'pinia'

export const topicStore = defineStore('topicStore', {

    state: () => {
        return { searchObject: {
                no: 1,
                size: 10,
                creator: '',
                topicCatalog: '',
                creatorName: ''
            } }
    },
    getters: {
        searchData: (state) => state.searchObject,
    },
    actions: {
        updateSearchObject(obj){
            this.searchObject=obj;
        }
    },
})
