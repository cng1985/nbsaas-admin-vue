# nbsaas基础vue管理后台模板

## 一、项目概述
**Nbsaas Admin Vue** 是一个基于 Vue.js 3.0 构建的轻量级后台管理系统，结合了现代前端技术栈的最佳实践，旨在帮助开发者快速构建具有高可扩展性和良好用户体验的后台管理系统。该项目拥有简洁的 UI 设计，强大的功能模块，支持多种自定义配置，适合电商、企业管理等各类业务场景。

## 二、主要特性
1. **Vue 3.x 支持**：基于最新的 Vue 3 构建，支持 Composition API 和全新的 Vue 生态体系。
2. **Element Plus**：采用了流行的 UI 框架 Element Plus 提供现代化的 UI 组件，方便快速构建响应式界面。
3. **Vue Router & Vuex**：内置强大的路由管理和状态管理功能，帮助实现复杂应用的路由和数据状态管理。
4. **权限控制**：支持基于角色的权限控制，可轻松扩展多角色、多权限的业务场景。
5. **丰富的组件库**：包括图表、表单、表格等常用组件，提升开发效率。
6. **良好的扩展性**：模块化设计，支持动态路由、插件机制，方便二次开发和功能扩展。
7. **响应式布局**：兼容各种终端设备，提供优质的用户体验。

## 编码规范

### 1.项目结构规范

```
{主工程}
{主工程}/assets             静态文件
{主工程}/components         公共组件
{主工程}/config             配置文件
{主工程}/layout             公共布局文件
{主工程}/mixins             混淆，vue3不采用了
{主工程}/router             路由配置
{主工程}/stores             pinia配置    
{主工程}/uses               公共vue3组合函数
{主工程}/utils              公共工具类
{主工程}/views              视图页面
{主工程}/views/common       基础视图
{主工程}/views/common       页面视图
```

### vue视图结构

```
/views/pages/业务模块/add.vue
/views/pages/业务模块/component/组件业务1.vue
/views/pages/业务模块/component/组件业务2.vue
/views/pages/业务模块/component/组件业务n.vue
/views/pages/业务模块/index.vue
/views/pages/业务模块/update.vue
/views/pages/业务模块/view.vue
/views/pages/业务模块/view_layout.vue
/views/pages/业务模块/view_业务1.vue
/views/pages/业务模块/view_业务2.vue
/views/pages/业务模块/view_业务n.vue

```

## 界面效果
![菜单管理功能](/documents/banner/1.png)
![模板管理功能](/documents/banner/2.png)
![插件管理功能](/documents/banner/3.png)
![通用查询功能](/documents/banner/4.png)

## 后台演示环境

http://adminstore.nbsaas.com 账号 admin 密码123456
## 依赖安装

```sh
pnpm i
```

### 开发调试

```sh
npm run dev
```

### 打包部署

```sh
npm run build
```

## 组合函数

### 分页组合函数

```js
import {usePage} from "@/utils/usePage";
const {pageData, sizeChange, pageChange, search, changeTableSort, loading} = usePage("/store/search", searchObject);
```

### 通用查询分页
```js
import {usePageData} from "@/uses/usePageData";
const {pageData, sizeChange, pageChange, search, changeTableSort, loading} = usePageData("page_vip_order", searchObject);
```

### 删除组合函数

```js
import {useDelete} from "@/utils/useDelete";
const {dialogVisible, deleteData, handleDelete} = useDelete("/store/delete", search);
```


## 主要功能模块

- 用户管理：包括用户的新增、编辑、删除、搜索等基本操作，同时支持用户角色分配。
- 权限管理：支持灵活的权限分配机制，可以对不同角色设置不同的访问权限。 
- 数据统计与分析：集成 ECharts，提供图表化的数据展示与统计功能。

## 技术栈

- 前端框架: Vue 3
- UI 框架: Element Plus
- 状态管理: Vuex
- 路由管理: Vue Router
- 打包工具: Vite

## 项目贡献

该项目为开源项目，欢迎开发者贡献代码。你可以通过以下方式参与：

- 提交 Issue：如果在使用过程中发现问题，欢迎提交 Issue。
- 提交 Pull Request：修复 Bug 或添加新功能后，可以通过 Pull Request 提交代码。
- 优化文档：帮助完善项目的中文和英文文档，让更多开发者了解并使用此项目。